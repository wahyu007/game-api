const jwt= require('jsonwebtoken');
const User = require('./../models').User;

module.exports = {
    verifyToken(req, res, next){
        let tokenHandler = req.headers['x-access-token'];

        if(!tokenHandler || tokenHandler.split(' ')[0] !== 'KEY'){
            return res.status(500).send({
                auth: false,
                message: "Error",
                errors: "Incorect token format"
            });
        }

        let token = tokenHandler.split(' ')[1];
        if(!token){
            return res.status(403).send({
                auth: false,
                message: "Error",
                errors: "No token provided"
            });
        }

        jwt.verify(token, "secretAuth", (err, decode) => {
            if(err){
                return res.status(500).send({
                    auth: false,
                    message: "Error",
                    errors: err
                });
            }

            req.userId = decode.id;
            next();
        });
    },
    
    isAdmin(req, res, next){
        User.findByPk(req.userId)
            .then(user => {
                console.log(user)
                user.getRoles().then(roles => {
                    console.log(roles[0].name);
                    if(roles[0].name == "ADMIN"){
                        next();
                        return;
                    }
                    res.status(403).send({
                        status: "error",
                        auth: false,
                        message: "Error",
                        message: 'Require Admin Role',
                    });

                    return;
                })
            })
    }
}
