// module.exports = (sequelize, Sequelize) => {
//     const User = sequelize.define("user_game_biodata", {
//         id_user : {
//             type : Sequelize.INTEGER,
//             primaryKey: true,
//             autoIncrement: true
//         },
        
//         name : {
//             type : Sequelize.STRING
//         },

//         email : {
//             type : Sequelize.STRING,
//             unique: true
//         },

//         password : {
//             type : Sequelize.STRING
//         },

//         address : {
//             type : Sequelize.STRING
//         },

//         sex : {
//             type : Sequelize.STRING
//         },

//         phone : {
//             type : Sequelize.STRING
//         }
//     });

//     User.associate = function(models){
//         User.belongToMany(models.Role, {
//             through: 'user_roles',
//             foreignKey: {
//                 name : 'id_user',
//                 type: Sequelize.INTEGER
//             },
//             otherKey: 'roleId'
//         })
//     }
//     return User;
// }

'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.belongsToMany(models.Role, {
        through: 'user_roles',
        foreignKey: {
            name : 'id_user',
            type: DataTypes.INTEGER
        },
        otherKey: 'roleId'
      })
    }
  };
  User.init({
    id_user : {
        type : DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    
    name : {
        type : DataTypes.STRING
    },

    email : {
        type : DataTypes.STRING,
        unique: true
    },

    password : {
        type : DataTypes.STRING
    },

    address : {
        type : DataTypes.STRING
    },

    sex : {
        type : DataTypes.STRING
    },

    phone : {
        type : DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};