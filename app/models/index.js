const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
// const config = require('../config/config');
const config = require(__dirname + '/../config/configDatabase.json')[env];
const Sequelize = require("sequelize");
const db = {};


let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}


fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
  });

  
Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
      db[modelName].associate(db);
    }
  });

// const sequelize = new Sequelize(
//     config.DB,
//     config.USER,
//     config.PASSWORD,
//     {
//         host: config.HOST,
//         dialect: config.dialect,
//         operatorsAliases: 0,
//         pool: {
//             max: config.pool.max,
//             min: config.pool.min,
//             acquire: config.pool.acquire,
//             idle: config.pool.idle
//         },
//         logging: true,
//     }
// );

// const Game = require('./games');
// const User = require('./users');

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// db.users = User(sequelize, Sequelize);
// db.games = Game(sequelize, Sequelize);
// db.logs = require("./logs")(sequelize, Sequelize);
// db.roles = require("./roles")(sequelize, Sequelize);

module.exports = db;
