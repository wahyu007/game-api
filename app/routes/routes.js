module.exports = app => {
    const users = require('../controllers/user');
    const games = require('../controllers/game');
    const logs = require('../controllers/log');
    const token = require('../controllers/token');

    let router = require('express').Router();

    router.post('/', users.create);
    router.post('/signup', users.signup);
    router.post('/login', users.login);

    router.get('/', [token.verifyToken, token.isAdmin], users.findAll);
    router.put('/:id', [token.verifyToken, token.isAdmin], users.update);
    router.delete('/:id', [token.verifyToken, token.isAdmin], users.delete);
    router.get('/games/all', [token.verifyToken, token.isAdmin], games.findAllGame);
    router.get('/profile/admin',[token.verifyToken, token.isAdmin], users.findUserProfile);

    router.get('/profile',[token.verifyToken], users.findUserProfile);
    router.post('/games',[token.verifyToken], games.create);
    router.post('/logs/create',[token.verifyToken], logs.create);
    router.get('/logs/all', [token.verifyToken], logs.findAllLog);
    router.get('/:id',[token.verifyToken], users.findOne);

    // app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON))
    app.use('/api/v1/users', router);
}